#!/bin/sh

set -xe

CONF_TPL_PATH='/v2ray/conf.tpl.jsonc'
CONF_PATH='/v2ray/config.jsonc'

if [ -z $DOCKER_ENV ]; then
  CONF_TPL_PATH='v2ray/conf.tpl.jsonc'
  CONF_PATH='v2ray/conf.jsonc'
  rm -f $CONF_PATH
fi

if [ ! -f $CONF_PATH ];then
  set +x
  tpl=$(cat $CONF_TPL_PATH)
  tpl=${tpl//'{{VMESS_ID}}'/${VMESS_ID:-'380cd246-9bcd-41e7-bc4f-5123aab84705'}}
  tpl=${tpl//'{{PROXY_PATH}}'/${PROXY_PATH:-'/4679'}}
  set -x
  echo "$tpl" > $CONF_PATH
fi

exec $@
